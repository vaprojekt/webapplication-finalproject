import { Injectable } from '@angular/core';
import { LocalStorageUpdateService } from './local-storage-update.service';

@Injectable({
  providedIn: 'root',
})
export class LocalStorageService {
  constructor(private localStorageUpdateService: LocalStorageUpdateService) {}

  get(key: string): any {
    const item = localStorage.getItem(key);
    if (item) {
      return JSON.parse(item);
    }
    return null;
  }

  set(key: string, value: any): void {
    localStorage.setItem(key, JSON.stringify(value));
    this.localStorageUpdateService.localStorageUpdated.emit();
  }

  remove(key: string): void {
    localStorage.removeItem(key);
    this.localStorageUpdateService.localStorageUpdated.emit();
  }

  clearAll(): void {
    // Clear all data from LocalStorage
    localStorage.clear();
    this.localStorageUpdateService.localStorageUpdated.emit();
  }
  
}