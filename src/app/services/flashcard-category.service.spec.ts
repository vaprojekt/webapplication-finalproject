import { TestBed } from '@angular/core/testing';

import { FlashcardCategoryService } from './flashcard-category.service';

describe('FlashcardCategoryService', () => {
  let service: FlashcardCategoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FlashcardCategoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
