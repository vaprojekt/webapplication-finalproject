import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  // https://www.telerik.com/blogs/angular-basics-how-to-use-httpclient
  // private _url: string = '/assets/data/flashcards.json';
  private _url = 'https://my-json-server.typicode.com/JSGund/XHR-Fetch-Request-JavaScript/posts';

  // Duden API sKZr6AMb7GaU654EqtE4K3xAlXs2hGze1b4bgQbi
  // This allow client to make request to fetch data
  constructor(private http: HttpClient) { }

  getWordInfos(languageCode: string, searchWord: string) {
    if(languageCode=='EN') {
      this._url = `https://api.dictionaryapi.dev/api/v2/entries/en/${searchWord}`;
    } else if (languageCode=='DE') {
      this._url = `https://api.dictionaryapi.dev/api/v2/entries/en/${searchWord}`;
    }
    return this.http.get(this._url).pipe(
      catchError((error) => {
        // Handle the error here (Extract the error object's attributes)
        const statusText = error.error.message;
        console.error(`HTTP Error: ${statusText}`);
  
        // Re-throw the error to pass it to the subscriber
        return throwError(statusText);
      })
    );
  }
  

}
