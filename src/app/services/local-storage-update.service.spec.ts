import { TestBed } from '@angular/core/testing';

import { LocalStorageUpdateService } from './local-storage-update.service';

describe('LocalStorageUpdateService', () => {
  let service: LocalStorageUpdateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LocalStorageUpdateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
