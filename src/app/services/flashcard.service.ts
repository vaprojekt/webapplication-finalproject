import { Injectable } from '@angular/core';
import { LocalStorageService } from './local-storage.service';
import { Flashcard } from '../models/flashcard.model';

@Injectable({
  providedIn: 'root',
})
export class FlashcardService {
  private flashcardsKey = 'flashcards';

  constructor(private localStorageService: LocalStorageService) {}

  getAllFlashcards(): Flashcard[] {
    return this.localStorageService.get(this.flashcardsKey) || [];
  }

  getFlashcardsByCategory(category: string): Flashcard[] {
    const allFlashcards = this.getAllFlashcards();
    return allFlashcards.filter((flashcard) => flashcard.category === category);
  }

  addFlashcard(flashcard: Flashcard): void {
    const allFlashcards = this.getAllFlashcards();
    allFlashcards.push(flashcard);
    this.localStorageService.set(this.flashcardsKey, allFlashcards);
  }

  updateFlashcard(updatedFlashcard: Flashcard): void {
    const allFlashcards = this.getAllFlashcards();
    const index = allFlashcards.findIndex((flashcard) => flashcard.id === updatedFlashcard.id);
    if (index !== -1) {
      allFlashcards[index] = updatedFlashcard;
      this.localStorageService.set(this.flashcardsKey, allFlashcards);
    }
  }

  updateFlashcards(updatedFlashcards: Flashcard[]): void {
    const allFlashcards = this.getAllFlashcards();
    updatedFlashcards.forEach((updatedFlashcard) => {
      const index = allFlashcards.findIndex((flashcard) => flashcard.id === updatedFlashcard.id);
      if (index !== -1) {
        allFlashcards[index] = updatedFlashcard;
      }
    });
    this.localStorageService.set(this.flashcardsKey, allFlashcards);
  }
  
  deleteFlashcard(id: string): void {
    const allFlashcards = this.getAllFlashcards();
    const updatedFlashcards = allFlashcards.filter((flashcard) => flashcard.id !== id);
    this.localStorageService.set(this.flashcardsKey, updatedFlashcards);
  }

  hasFlashcards(category: string): boolean {
    const flashcardsInCategory = this.getFlashcardsByCategory(category);
    return flashcardsInCategory.length > 0;
  }
}
