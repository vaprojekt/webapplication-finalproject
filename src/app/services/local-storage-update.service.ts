import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class LocalStorageUpdateService {
  public localStorageUpdated: EventEmitter<void> = new EventEmitter<void>();

  constructor() {}
}
