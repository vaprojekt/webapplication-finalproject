import { Injectable } from '@angular/core';
import { LocalStorageService } from './local-storage.service';

@Injectable({
  providedIn: 'root',
})
export class FlashcardCategoryService {
  private categoriesKey = 'flashcardCategories';

  constructor(private localStorageService: LocalStorageService) {}

  getAllCategories(): string[] {
    return this.localStorageService.get(this.categoriesKey) || [];
  }

  addCategory(category: string): void {
    const allCategories = this.getAllCategories();
    allCategories.push(category);
    this.localStorageService.set(this.categoriesKey, allCategories);
  }

  updateCategory(oldCategory: string, newCategory: string): void {
    const allCategories = this.getAllCategories();
    const index = allCategories.indexOf(oldCategory);
    if (index !== -1) {
      allCategories[index] = newCategory;
      this.localStorageService.set(this.categoriesKey, allCategories);
    }
  }

  deleteCategory(category: string): void {
    const allCategories = this.getAllCategories();
    const updatedCategories = allCategories.filter((cat) => cat !== category);
    this.localStorageService.set(this.categoriesKey, updatedCategories);
  }
}
