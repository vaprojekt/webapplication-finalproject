import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateUpdateFlashcardModalPageRoutingModule } from './create-update-flashcard-modal-routing.module';

import { CreateUpdateFlashcardModalPage } from './create-update-flashcard-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateUpdateFlashcardModalPageRoutingModule
  ],
  declarations: [CreateUpdateFlashcardModalPage]
})
export class CreateUpdateFlashcardModalPageModule {}
