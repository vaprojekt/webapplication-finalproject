import { Component, OnInit } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { FlashcardCategoryService } from '../../services/flashcard-category.service';

@Component({
  selector: 'app-create-update-flashcard-modal',
  templateUrl: './create-update-flashcard-modal.page.html',
  styleUrls: ['./create-update-flashcard-modal.page.scss'],
})
export class CreateUpdateFlashcardModalPage implements OnInit {
  flashcard = {
    category: '',
    word: '',
    exampleSentence: '',
  };

  categories: string[] = [];
  newWord: string = '';
  newExampleSentence: string = '';

  constructor(
    private modalCtrl: ModalController, 
    private flashcardCategoryService: FlashcardCategoryService,
    private alertCtrl: AlertController,
  ) {}

  ngOnInit() {
    // Get the list of categories from the FlashcardCategoryService
    this.categories = this.flashcardCategoryService.getAllCategories();
  }

  cancel() {
    return this.modalCtrl.dismiss(null, 'cancel');
  }

  confirm() {
    // Check if word and category and exampleSentence are provided
    if (!this.newWord || !this.flashcard.category || !this.newExampleSentence) {
      // Display an alert if one of field not provided
      this.showAlert('Warning', 'All field must filled before confirm.');
      return;
    }

    // Set the values and send them back to the FlashcardPage
    this.flashcard.category = this.flashcard.category
    this.flashcard.word = this.newWord;
    this.flashcard.exampleSentence = this.newExampleSentence;
    return this.modalCtrl.dismiss(this.flashcard, 'confirm');
  }

  private async showAlert(header: string, message: string) {
    // Using Ionic alert to display a warning popup
    const alert = await this.alertCtrl.create({
      header: header,
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }

}
