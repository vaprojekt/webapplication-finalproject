import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateUpdateFlashcardModalPage } from './create-update-flashcard-modal.page';

const routes: Routes = [
  {
    path: '',
    component: CreateUpdateFlashcardModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateUpdateFlashcardModalPageRoutingModule {}
