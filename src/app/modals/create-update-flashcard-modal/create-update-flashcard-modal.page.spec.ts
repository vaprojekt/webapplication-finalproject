import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CreateUpdateFlashcardModalPage } from './create-update-flashcard-modal.page';

describe('CreateUpdateFlashcardModalPage', () => {
  let component: CreateUpdateFlashcardModalPage;
  let fixture: ComponentFixture<CreateUpdateFlashcardModalPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(CreateUpdateFlashcardModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
