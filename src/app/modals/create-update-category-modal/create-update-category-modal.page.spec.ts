import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CreateUpdateCategoryModalPage } from './create-update-category-modal.page';

describe('CreateUpdateCategoryModalPage', () => {
  let component: CreateUpdateCategoryModalPage;
  let fixture: ComponentFixture<CreateUpdateCategoryModalPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(CreateUpdateCategoryModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
