import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateUpdateCategoryModalPageRoutingModule } from './create-update-category-modal-routing.module';

import { CreateUpdateCategoryModalPage } from './create-update-category-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateUpdateCategoryModalPageRoutingModule
  ],
  declarations: [CreateUpdateCategoryModalPage]
})
export class CreateUpdateCategoryModalPageModule {}
