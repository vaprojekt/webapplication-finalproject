import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-create-update-category-modal',
  templateUrl: './create-update-category-modal.page.html',
  styleUrls: ['./create-update-category-modal.page.scss'],
})
export class CreateUpdateCategoryModalPage implements OnInit {
  categoryName: string = '';

  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {
  }

  cancel() {
    return this.modalCtrl.dismiss(null, 'cancel');
  }

  confirm() {
    return this.modalCtrl.dismiss(this.categoryName, 'confirm');
  }
}