import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateUpdateCategoryModalPage } from './create-update-category-modal.page';

const routes: Routes = [
  {
    path: '',
    component: CreateUpdateCategoryModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateUpdateCategoryModalPageRoutingModule {}
