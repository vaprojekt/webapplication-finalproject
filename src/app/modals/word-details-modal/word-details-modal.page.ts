import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { LocalStorageService } from '../../services/local-storage.service';

@Component({
  selector: 'app-word-details-modal',
  templateUrl: './word-details-modal.page.html',
  styleUrls: ['./word-details-modal.page.scss'],
})
export class WordDetailsModalPage implements OnInit {
  @Input() selectedWord: any;
  isBookmarked: boolean = false; // Initialize to false by default

  constructor(
    private modalController: ModalController,
    private localStorageService: LocalStorageService  // Inject the LocalStorageService
  ) {}

  ngOnInit() {
    // Check if the word is already bookmarked and update isBookmarked accordingly
    const bookmarkedWords = this.localStorageService.get('bookmarkedWords') || [];
    this.isBookmarked = bookmarkedWords.some((word: { word: any; }) => word.word === this.selectedWord.word);
  }

  closeModal() {
    // Dismiss the modal without passing any data
    this.modalController.dismiss();
  }

  addToBookmark() {
    // Toggle the bookmark status
    this.isBookmarked = !this.isBookmarked;

    // Retrieve existing bookmarked words from local storage
    let bookmarkedWords = this.localStorageService.get('bookmarkedWords') || [];

    if (this.isBookmarked) {
      // Add the word to the bookmarked list
      bookmarkedWords.push(this.selectedWord);
    } else {
      // Remove the word from the bookmarked list
      bookmarkedWords = bookmarkedWords.filter((word: { word: any; }) => word.word !== this.selectedWord.word);
    }

    // Update the local storage with the modified list of bookmarked words
    this.localStorageService.set('bookmarkedWords', bookmarkedWords);

  }
}
