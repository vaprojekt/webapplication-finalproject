import { ComponentFixture, TestBed } from '@angular/core/testing';
import { WordDetailsModalPage } from './word-details-modal.page';

describe('WordDetailsModalPage', () => {
  let component: WordDetailsModalPage;
  let fixture: ComponentFixture<WordDetailsModalPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(WordDetailsModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
