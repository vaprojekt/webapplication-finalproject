import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WordDetailsModalPageRoutingModule } from './word-details-modal-routing.module';

import { WordDetailsModalPage } from './word-details-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WordDetailsModalPageRoutingModule
  ],
  declarations: [WordDetailsModalPage]
})
export class WordDetailsModalPageModule {}
