import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WordDetailsModalPage } from './word-details-modal.page';

const routes: Routes = [
  {
    path: '',
    component: WordDetailsModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WordDetailsModalPageRoutingModule {}
