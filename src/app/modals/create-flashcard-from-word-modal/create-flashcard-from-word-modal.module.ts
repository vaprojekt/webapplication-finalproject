import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateFlashcardFromWordModalPageRoutingModule } from './create-flashcard-from-word-modal-routing.module';

import { CreateFlashcardFromWordModalPage } from './create-flashcard-from-word-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateFlashcardFromWordModalPageRoutingModule
  ],
  declarations: [CreateFlashcardFromWordModalPage]
})
export class CreateFlashcardFromWordModalPageModule {}
