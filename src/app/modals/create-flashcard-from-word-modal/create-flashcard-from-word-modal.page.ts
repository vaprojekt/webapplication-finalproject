import { Component, OnInit } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { FlashcardCategoryService } from '../../services/flashcard-category.service';
import { FlashcardService } from '../../services/flashcard.service';

@Component({
  selector: 'app-create-flashcard-from-word-modal',
  templateUrl: './create-flashcard-from-word-modal.page.html',
  styleUrls: ['./create-flashcard-from-word-modal.page.scss'],
})
export class CreateFlashcardFromWordModalPage implements OnInit {
  selectedCategory: string = '';
  categories: string[] = [];
  word: string = '';

  constructor(
    private modalCtrl: ModalController, 
    private flashcardCategoryService: FlashcardCategoryService,
    private alertCtrl: AlertController,
    private flashcardService: FlashcardService,
  ) {}

  ngOnInit() {
    // Get the list of categories from the FlashcardCategoryService
    this.categories = this.flashcardCategoryService.getAllCategories();
    // Retrieve the word from the route parameters
    console.log('data passed to modal: ',this.word);
  }

  cancel() {
    return this.modalCtrl.dismiss(null, 'cancel');
  }

  confirm() {
    // Check if category selected
    if (!this.selectedCategory) {
      // Display an alert if one of field not provided
      this.showAlert('Warning', 'You must select catagory before confirm.');
      return;
    }

    // Check if the word already exists in any flashcard of the selected category
    const flashcardsInSelectedCategory = this.flashcardService.getFlashcardsByCategory(this.selectedCategory);
    const wordExists = flashcardsInSelectedCategory.some(
      flashcard => flashcard.word.toLowerCase() === this.word.toLowerCase()
    );
    
    if (wordExists) {
      // Display an alert if the flashcard word already exists in the selected category
      this.showAlert('Warning', 'The word already exists in another flashcard in the selected category.');
      return;
    }

    // Send data back to the Search Page
    return this.modalCtrl.dismiss(this.selectedCategory, 'confirm');

  }

  private async showAlert(header: string, message: string) {
    // Using Ionic alert to display a warning popup
    const alert = await this.alertCtrl.create({
      header: header,
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }
}
