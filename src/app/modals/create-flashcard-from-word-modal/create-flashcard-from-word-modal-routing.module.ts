import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateFlashcardFromWordModalPage } from './create-flashcard-from-word-modal.page';

const routes: Routes = [
  {
    path: '',
    component: CreateFlashcardFromWordModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateFlashcardFromWordModalPageRoutingModule {}
