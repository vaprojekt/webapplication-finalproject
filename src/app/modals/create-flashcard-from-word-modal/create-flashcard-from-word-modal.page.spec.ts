import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CreateFlashcardFromWordModalPage } from './create-flashcard-from-word-modal.page';

describe('CreateFlashcardFromWordModalPage', () => {
  let component: CreateFlashcardFromWordModalPage;
  let fixture: ComponentFixture<CreateFlashcardFromWordModalPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(CreateFlashcardFromWordModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
