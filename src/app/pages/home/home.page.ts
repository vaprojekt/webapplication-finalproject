import { Component, OnInit, OnDestroy } from '@angular/core';
import { FlashcardCategoryService } from '../../services/flashcard-category.service';
import { ModalController, AlertController } from '@ionic/angular';
import { CreateUpdateCategoryModalPage } from '../../modals/create-update-category-modal/create-update-category-modal.page';
import { FlashcardService } from '../../services/flashcard.service';
import { Router, NavigationExtras } from '@angular/router';
import { Subscription } from 'rxjs';
import { LocalStorageUpdateService } from '../../services/local-storage-update.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit, OnDestroy {
  title = 'VocabVault';
  categories: string[] = [];
  newCategory: string = '';
  localStorageUpdateSubscription: Subscription;

  constructor(
    private flashcardCategoryService: FlashcardCategoryService,
    private modalCtrl: ModalController,
    private alertCtrl: AlertController,
    private flashcardService: FlashcardService,
    private router: Router,
    private localStorageUpdateService: LocalStorageUpdateService,
  ) {
    // Subscribe to the localStorageUpdated event
    this.localStorageUpdateSubscription = this.localStorageUpdateService.localStorageUpdated.subscribe(() => {
      this.updateCategories();
    });
  }

  ngOnInit() {
    this.categories = this.flashcardCategoryService.getAllCategories();
  }

  ngOnDestroy() {
    // Unsubscribe to avoid memory leaks
    this.localStorageUpdateSubscription.unsubscribe();
  }

  private updateCategories() {
    // Update the categories array
    this.categories = this.flashcardCategoryService.getAllCategories();
  }

  getFlashcardCount(category: string): number {
    return this.flashcardService.getFlashcardsByCategory(category).length;
  }
  
  addCategory() {
    if (this.newCategory) {
      // Check if the category already exists
      if (!this.categories.includes(this.newCategory)) {
        // Add the category if it doesn't exist
        this.flashcardCategoryService.addCategory(this.newCategory);
        this.newCategory = '';
        this.categories = this.flashcardCategoryService.getAllCategories();
      } else {
        // Display an alert if the category already exists
        this.showAlert('Warning', 'The entered category already exists.');
      }
    }
  }

  private async showAlert(header: string, message: string) {
    // Using Ionic alert to display a warning popup
    const alert = await this.alertCtrl.create({
      header: header,
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }

  async updateCategory(category: string, newCategory: string) {
    // Check if the newCategory already exists in the list
    if (!this.categories.includes(newCategory)) {
      // Get flashcards in the old category
      const flashcardsToUpdate = this.flashcardService.getFlashcardsByCategory(category);

      // Update the category attribute for each flashcard
      flashcardsToUpdate.forEach(flashcard => flashcard.category = newCategory);

      // Update the category itself
      this.flashcardService.updateFlashcards(flashcardsToUpdate);
      this.flashcardCategoryService.updateCategory(category, newCategory);
      this.categories = this.flashcardCategoryService.getAllCategories();
    } else {
      // Display an alert if the category already exists
      this.showAlert('Warning', 'The entered category already exists.');
    }
  }

  async deleteCategory(category: string) {
    // Check if the category has flashcards
    const hasFlashcards = this.flashcardService.hasFlashcards(category);

    if (hasFlashcards) {
      // Display alert for non-empty category
      this.showAlert('Warning', 'This category has flashcards. Action not allowed');

    } else {
      // Perform delete for empty category
      // Display a confirmation alert before deleting category
      const alert = await this.alertCtrl.create({
        header: 'Warning',
        message: 'Are you sure you want to delete it?',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
          },
          {
            text: 'Delete',
            handler: () => {
              this.performDeleteCategory(category);
            },
          },
        ],
      });

      await alert.present();   
    }
  }

  private performDeleteCategory(category: string) {
    this.flashcardCategoryService.deleteCategory(category);
    this.categories = this.flashcardCategoryService.getAllCategories();
  }

  // Handle open modal and get data when confirm or dismiss
  async openCreateUpdateCategoryModal(category: string | null = null) {
    const modal = await this.modalCtrl.create({
      component: CreateUpdateCategoryModalPage,
    });
    modal.present();

    const { data, role } = await modal.onWillDismiss();

    if (role === 'confirm' && data) {
      if (category) {
        // Update category with new name (data) if category is provided
        this.updateCategory(category, data);
      } else {
        // Add new category if no category is provided
        this.newCategory = data;
        this.addCategory();
      }
    }
  }

  redirectToFlashcardPage(category: string) {
    // Redirect to the flashcard page and send data with navigationExtras
    const navigationExtras: NavigationExtras = {
      state: {
        category: category,
      },
    };
  
    this.router.navigate(['flashcards'], navigationExtras);
  }

}
