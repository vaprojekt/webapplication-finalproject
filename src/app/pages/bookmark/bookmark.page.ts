import { Component, OnInit, OnDestroy } from '@angular/core';
import { LocalStorageService } from '../../services/local-storage.service';
import { ModalController } from '@ionic/angular';
import { WordDetailsModalPage } from '../../modals/word-details-modal/word-details-modal.page';
import { LocalStorageUpdateService } from '../../services/local-storage-update.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-bookmark',
  templateUrl: './bookmark.page.html',
  styleUrls: ['./bookmark.page.scss'],
})
export class BookmarkPage implements OnInit, OnDestroy {
  bookmarkedWords: any[] = [];
  private localStorageSubscription: Subscription = new Subscription();

  constructor(
    private localStorageService: LocalStorageService,
    private modalController: ModalController,
    private localStorageUpdateService: LocalStorageUpdateService
  ) {}

  ngOnInit() {
    // Retrieve bookmarked words from local storage
    this.bookmarkedWords = this.localStorageService.get('bookmarkedWords') || [];

    // Subscribe to local storage update events
    this.localStorageSubscription = this.localStorageUpdateService.localStorageUpdated.subscribe(() => {
      this.updateBookmarkedWords();
    });
  }

  ngOnDestroy() {
    // Unsubscribe from local storage update events to prevent memory leaks
    this.localStorageSubscription.unsubscribe();
  }

  private updateBookmarkedWords() {
    // Update bookmarked words when local storage is updated
    this.bookmarkedWords = this.localStorageService.get('bookmarkedWords') || [];
  }

  async openViewWordDetailsModal(selectedWord?: any) {
    const modal = await this.modalController.create({
      component: WordDetailsModalPage,
      componentProps: {
        selectedWord: selectedWord,
      },
    });

    modal.onDidDismiss().then((result) => {
      // Handle modal dismissal if needed
    });

    return await modal.present();
  }
}
