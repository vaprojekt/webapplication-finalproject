import { Component, OnInit } from '@angular/core';
import { FlashcardService } from '../../services/flashcard.service';
import { Flashcard } from '../../models/flashcard.model';
import { FlashcardCategoryService } from '../../services/flashcard-category.service';
import { ModalController, AlertController } from '@ionic/angular';
import { CreateUpdateFlashcardModalPage } from '../../modals/create-update-flashcard-modal/create-update-flashcard-modal.page';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-flashcards',
  templateUrl: './flashcards.page.html',
  styleUrls: ['./flashcards.page.scss'],
})
export class FlashcardsPage implements OnInit {
  flashcards: Flashcard[] = [];
  category: string = '';
  flippedCards: Set<string> = new Set<string>();

  constructor(
    private flashcardCategoryService: FlashcardCategoryService,
    private modalCtrl: ModalController,
    private alertCtrl: AlertController,
    private flashcardService: FlashcardService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  // Method to get a random category
  private getRandomCategory(): string {
    const allCategories = this.flashcardCategoryService.getAllCategories();
    const randomIndex = Math.floor(Math.random() * allCategories.length);
    return allCategories[randomIndex];
  }

  ngOnInit() {
    // Get the category from the route parameters or state
    this.route.params.subscribe(params => {
      this.category = params['category'];
      // console.log('OnInit category:', this.category);
      // If no category is provided, get a random one
      if (!this.category) {
        // Check if there is state data
        const state = this.router.getCurrentNavigation()?.extras.state;
        // console.log('OnInit state:', state);
        if (state && state['category']) {
          this.category = state['category'];
          // console.log('OnInit category from state:', this.category);
        } else {
          this.category = this.getRandomCategory();
          // console.log('OnInit get random category:', this.category);
        }
      }
      // Get all Flashcards by category
      this.flashcards = this.flashcardService.getFlashcardsByCategory(this.category);
    });
  }

  // Toggle the flip state for a specific card
  flipCard(flashcard: Flashcard) {
    const cardId = flashcard.id;
    if (this.flippedCards.has(cardId)) {
      this.flippedCards.delete(cardId);
    } else {
      this.flippedCards.add(cardId);
    }
  }

  // Check if a card is flipped
  isCardFlipped(flashcard: Flashcard): boolean {
    return this.flippedCards.has(flashcard.id);
  }

  addFlashcard(flashcardAttributes: any) {

    // Check if the word already exists in any flashcard of the selected category
    const flashcardsInSelectedCategory = this.flashcardService.getFlashcardsByCategory(flashcardAttributes.category);
    const wordExists = flashcardsInSelectedCategory.some(
      flashcard => flashcard.word.toLowerCase() === flashcardAttributes.word.toLowerCase()
    );

    if (!wordExists) {
      // Add the flashcard to the selected category if it doesn't exist
      const newFlashcard: Flashcard = {
        id: new Date().getTime().toString(),
        category: flashcardAttributes.category,
        word: flashcardAttributes.word,
        exampleSentence: flashcardAttributes.exampleSentence,
      };
      this.flashcardService.addFlashcard(newFlashcard);
      this.flashcards = this.flashcardService.getFlashcardsByCategory(this.category);
    } else {
      // Display an alert if the flashcard word already exists in the selected category
      this.showAlert('Warning', 'The entered word already exists in another flashcard in the selected category.');
    }

  }

  // Handle open modal and get data when confirm or dismiss
  async openCreateUpdateFlashcardModal(flashcard: Flashcard | null = null) {
    // While adding new flashcard we must have a least one category to select
    // no flashcard is provided as parameter
    if (!flashcard) {
      if (this.flashcardCategoryService.getAllCategories().length == 0) {
        // Display an alert if the are no category
        this.showAlert('Warning', 'No category found. You must have at least one category to create flashcard.');
        return;
      }
    }

    const modal = await this.modalCtrl.create({
      component: CreateUpdateFlashcardModalPage,
    });
    modal.present();

    const { data, role } = await modal.onWillDismiss();

    if (role === 'confirm' && data) {
      // Process the data returned from the modal
      console.log('Flashcard data:', data);
      if (flashcard) {
        // Update flashcard's attribute with new attributes(data) if flashcard is provided
        this.updateFlashcard(flashcard, data);
      } else {
        // Add new flashcard if no flashcard is provided
        this.addFlashcard(data);
      }
    }
  }


  private async showAlert(header: string, message: string) {
    // Using Ionic alert to display a warning popup
    const alert = await this.alertCtrl.create({
      header: header,
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }

  async updateFlashcard(flashcard: Flashcard, newFlashcardAttributes: any) {
    // Check if the word already exists in any flashcard of the selected category
    const flashcardsInSelectedCategory = this.flashcardService.getFlashcardsByCategory(newFlashcardAttributes.category);
    const wordExists = flashcardsInSelectedCategory.some(
      flashcard => flashcard.word.toLowerCase() === newFlashcardAttributes.word.toLowerCase()
    );

    if (!wordExists) {
      // Update corresponding attributes for the flashcard
      flashcard.category = newFlashcardAttributes.category;
      flashcard.word = newFlashcardAttributes.word;
      flashcard.exampleSentence = newFlashcardAttributes.exampleSentence;

      this.flashcardService.updateFlashcard(flashcard);
      this.flashcards = this.flashcardService.getFlashcardsByCategory(this.category);
    } else {
      // Display an alert if the flashcard word already exists in the selected category
      this.showAlert('Warning', 'The entered word already exists in another flashcard in the selected category.');
    }
  }

  async deleteFlashcard(id: string) {
    // Perform delete for flashcard
    // Display a confirmation alert before delete flashcard
    const alert = await this.alertCtrl.create({
      header: 'Warning',
      message: 'Are you sure you want to delete it?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Delete',
          handler: () => {
            this.performDeleteFlashcard(id);
          },
        },
      ],
    });

    await alert.present();
  }

  private performDeleteFlashcard(id: string) {
    this.flashcardService.deleteFlashcard(id);
    this.flashcards = this.flashcardService.getFlashcardsByCategory(this.category);
  }


}
