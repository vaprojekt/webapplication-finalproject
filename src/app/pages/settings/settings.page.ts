import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { LocalStorageService } from '../../services/local-storage.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  constructor(
    private alertController: AlertController,
    private localStorageService: LocalStorageService,
  ) {}

  ngOnInit() {
  }
  async showResetConfirmation() {
    const alert = await this.alertController.create({
      header: 'Reset Confirmation',
      message: 'All data will be reset. Do you really want to continue?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
        },
        {
          text: 'Yes',
          handler: () => {
            this.resetLocalStorageData();
          },
        },
      ],
    });

    await alert.present();
  }

  resetLocalStorageData() {
    // Clear all data from LocalStorage
    this.localStorageService.clearAll();
  }

}
