import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../services/http.service';
import { Word } from '../../models/word.model';
import { Flashcard } from '../../models/flashcard.model';
import { FlashcardCategoryService } from '../../services/flashcard-category.service';
import { FlashcardService } from '../../services/flashcard.service';
import { ModalController, AlertController } from '@ionic/angular';
import { WordDetailsModalPage } from 'src/app/modals/word-details-modal/word-details-modal.page';
import { CreateFlashcardFromWordModalPage } from 'src/app/modals/create-flashcard-from-word-modal/create-flashcard-from-word-modal.page';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})

export class SearchPage implements OnInit {
  selectedLanguage: string = '';
  searchWord: string = '';
  showError: boolean = false;
  wordResults: Word[] = [];
  errorMsg: string = '';

  // List of random words
  wordList: string[] = [
    'elephant',
    'paradox',
    'stellar',
    'benevolent',
    'quixotic',
    'ephemeral',
    'serendipity',
    'labyrinth',
    'effervescent',
    'cacophony',
    'resplendent',
    'obfuscate',
    'denouement',
    'plethora',
    'mellifluous',
    'petrichor',
    'ambrosia',
    'nebulous',
    'quintessential',
    'ephemeral',
    'taciturn',
    'luminous',
    'effulgent',
    'magnanimous',
    'halcyon',
    'crepuscular',
    'susurrus',
    'incandescent',
    'peregrinate',
    'capitulate',
    'efficacious',
    'quandary',
    'disparate',
    'querulous',
    'gregarious',
    'facetious',
    'vicissitude',
  ];

  constructor(
    private httpService: HttpService,
    private modalController: ModalController,
    private flashcardCategoryService: FlashcardCategoryService,
    private alertCtrl: AlertController,
    private flashcardService: FlashcardService,
  ) { }

  ngOnInit() {
  }

  onInputChange(event: CustomEvent) {
    // Reset error message when the user starts typing
    this.showError = false;
  }

  searchWordDefinition() {
    // Check if language and search word are provided
    if (!this.selectedLanguage || !this.searchWord) {
      this.showError = true;
      return;
    }
    
    // Reset error message
    this.showError = false;
    this.errorMsg = '';
    
    // Perform the search using the HTTP service
    this.httpService.getWordInfos(this.selectedLanguage, this.searchWord)
      .subscribe((data: any) => {
        // Handle the data returned from the API
        console.log("received data: ", data);
        // Extract the desired information based on the Word model
        this.wordResults = (data as Word[]);
        console.log("extracted data: ", this.wordResults);
      }, (error) => {
        // Handle API error
        this.errorMsg = error;
        console.error(error);
        // Clear search results on error
        this.wordResults = [];
      });
  }

  lookupRandomWord() {
    // Check if language is provided
    if (!this.selectedLanguage) {
      this.showError = true;
      return;
    }

    // Reset error message
    this.showError = false;
    this.errorMsg = '';

    // Select a random word from the list
    const randomIndex = Math.floor(Math.random() * this.wordList.length);
    const randomWord = this.wordList[randomIndex];

    // Perform the search using the HTTP service
    this.httpService.getWordInfos(this.selectedLanguage, randomWord)
      .subscribe((data: any) => {
        // Handle the data returned from the API
        console.log("received data: ", data);
        // Extract the desired information based on the Word model
        this.wordResults = (data as Word[]);
        console.log("extracted data: ", this.wordResults);
      }, (error) => {
        // Handle API error
        this.errorMsg = error;
        console.error(error);
        // Clear search results on error
        this.wordResults = [];
      });
  }

  async openViewWordDetailsModal(selectedWord?: Word) {
    const modal = await this.modalController.create({
      component: WordDetailsModalPage,
      componentProps: {
        selectedWord: selectedWord || this.wordResults[0], // Pass the first word as a default
      },
    });

    modal.onDidDismiss().then((result) => {
      // Handle modal dismissal if needed
    });

    return await modal.present();
  }

  async openCreateFlashcardModal(word: Word) {
    // In Modal user must select a category to create flashcard for the word
    // so we must ensure there is at least one category exist
    if (this.flashcardCategoryService.getAllCategories().length == 0) {
      // Display an alert if the are no category
      this.showAlert('Warning', 'No category found. You must have at least one category to create flashcard.');
      return;
    }

    // open modal create flashcard from word
    const modal = await this.modalController.create({
      component: CreateFlashcardFromWordModalPage,
      componentProps: {
        // Pass the word to the modal
        word: word.word,
      },
    });

    modal.present();

    const { data, role } = await modal.onWillDismiss();

    if (role === 'confirm' && data) {
      // Process the data(category) returned from the modal
      console.log('Flashcard data:', data);
      // Add new flashcard
      this.addFlashcard(word, data);
      // Display an success message
      this.showAlert('Info', 'Flashcard created');
    }
  }
  
  private async showAlert(header: string, message: string) {
    // Using Ionic alert to display a warning popup
    const alert = await this.alertCtrl.create({
      header: header,
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }

  private addFlashcard(word: Word, category: string) {
    // Add the flashcard to the selected category if it doesn't exist
    const newFlashcard: Flashcard = {
      id: new Date().getTime().toString(),
      category: category,
      word: word.word,
      exampleSentence: this.getExampleSentence(word),
    };
    this.flashcardService.addFlashcard(newFlashcard);
  }
  
  private getExampleSentence(word: Word): string {
    let exampleSentence = 'No example found. This is the default sentence.';
  
    // Iterate through meanings
    for (const meaning of word.meanings) {
      // Iterate through definitions
      for (const definition of meaning.definitions) {
        // Check if there is an example sentence
        if (definition.example) {
          exampleSentence = definition.example;
          // Break out of both loops when an example is found
          break;
        }
      }
      // Break out of the outer loop when an example is found
      if (exampleSentence !== 'No example found. This is the default sentence.') {
        break;
      }
    }
  
    return exampleSentence;
  }  

}
