import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: '',
        //use url like domain.com/tabs/home or /tabs/settings or /tabs/search or /tabs/best-sellers then set redirectTo: '/tabs/home',
        redirectTo: '/home',
        pathMatch: 'full',
      },
      {
        path: 'home',
        loadChildren: () => import('../pages/home/home.module').then(m => m.HomePageModule)
      },
      {
        path: 'search',
        loadChildren: () => import('../pages/search/search.module').then(m => m.SearchPageModule)
      },
      {
        path: 'settings',
        loadChildren: () => import('../pages/settings/settings.module').then(m => m.SettingsPageModule)
      },
      {
        path: 'bookmark',
        loadChildren: () => import('../pages/bookmark/bookmark.module').then(m => m.BookmarkPageModule)
      },
      {
        path: 'flashcards',
        loadChildren: () => import('../pages/flashcards/flashcards.module').then(m => m.FlashcardsPageModule)
      },
      //False url will be redirected to home page, can edit to redirect to page-not-found if need
      {
        path: '**',
        redirectTo: '/home',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class TabsPageRoutingModule {}
