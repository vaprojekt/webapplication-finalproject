export interface Flashcard {
    id: string;
    category: string;
    word: string;
    exampleSentence: string;
}