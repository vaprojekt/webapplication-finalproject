export interface Word {
    word: string;
    phonetics: Phonetics[];
    meanings: Meaning[];
    origin: string;
  }
  
  interface Phonetics {
    text: string;
  }
  
  interface Meaning {
    partOfSpeech: string;
    definitions: Definition[];
    synonyms: string[];
    antonyms: string[];
  }
  
  interface Definition {
    definition: string;
    synonyms: string[];
    antonyms: string[];
    example?: string;
  }

  