import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  //use url like domain.com/home or /settings or /search or /flashcards
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'word-details-modal',
    loadChildren: () => import('./modals/word-details-modal/word-details-modal.module').then( m => m.WordDetailsModalPageModule)
  },
  {
    path: 'create-update-category-modal',
    loadChildren: () => import('./modals/create-update-category-modal/create-update-category-modal.module').then( m => m.CreateUpdateCategoryModalPageModule)
  },
  {
    path: 'create-update-flashcard-modal',
    loadChildren: () => import('./modals/create-update-flashcard-modal/create-update-flashcard-modal.module').then( m => m.CreateUpdateFlashcardModalPageModule)
  },
  {
    path: 'create-flashcard-from-word-modal',
    loadChildren: () => import('./modals/create-flashcard-from-word-modal/create-flashcard-from-word-modal.module').then( m => m.CreateFlashcardFromWordModalPageModule)
  },
  //Add another route as need (example modal)
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
